.. Debian packaging tips documentation master file, created by
   sphinx-quickstart on Mon Apr 15 07:54:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Debian packaging tips's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   workflow

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
